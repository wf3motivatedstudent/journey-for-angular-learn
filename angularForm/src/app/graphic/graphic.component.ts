import { Component, OnInit } from '@angular/core';
import { UsersService } from '../service/users.service';
import { User } from '../interface/user';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.css']
})
export class GraphicComponent implements OnInit {

  users: Array<User> = [];

  constructor( private usersService: UsersService ) {

  }


  ngOnInit() {
    this.users = this.usersService.users;
  }

}
