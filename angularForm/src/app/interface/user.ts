export interface User {
  name: String;
  firstName: String;
  onInternet: Number;
  appreciated: Number;
  recommendation: Boolean;
}
