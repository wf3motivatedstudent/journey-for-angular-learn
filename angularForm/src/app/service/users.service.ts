import { Injectable } from '@angular/core';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: Array<User> = [{
    name: 'Polob',
    firstName: 'Bloufpif',
    onInternet: 2,
    appreciated: 60,
    recommendation: true
  }, {
    name: 'Minou',
    firstName: 'Chatouner',
    onInternet: 72,
    appreciated: 100,
    recommendation: false
  }, {
    name: 'Polob',
    firstName: 'Bloufpif',
    onInternet: 2,
    appreciated: 60,
    recommendation: true
  }, {
    name: 'Minou',
    firstName: 'Chatouner',
    onInternet: 72,
    appreciated: 100,
    recommendation: false
  }];
  constructor() { }
}

