import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { GraphicComponent } from './graphic/graphic.component';
import { FormsModule } from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import { FormComponent } from './form/form.component';

const appRoutes: Routes = [
  { path: 'form', component: FormComponent},
  { path: 'userList', component: UserListComponent},
  { path: 'graphic', component: GraphicComponent},
  { path: '', component: FormComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    GraphicComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
