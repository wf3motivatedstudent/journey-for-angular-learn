import { Component, OnInit } from '@angular/core';
import { UsersService } from '../service/users.service';
import { User } from '../interface/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {

  users: Array<User> = [];

  constructor( private usersService: UsersService ) { }

  ngOnInit() {
    this.users = this.usersService.users;
  }

}
