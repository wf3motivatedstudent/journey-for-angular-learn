import { Component, OnInit } from '@angular/core';
import { User } from '../interface/user';
import { UsersService } from '../service/users.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  users: Array<User> = [];

  addUser: User = {
    name: '',
    firstName: '',
    onInternet: 0,
    appreciated: 0,
    recommendation: true
  };

  constructor( private usersService: UsersService ) { }

  onSubmit() {
    this.users.push({ ... this.addUser }); // ICI on push addUser dans le clone tableau users (dans le service)

    // ICI on reset le formulaire
    this.addUser = {
      name: '',
      firstName: '',
      onInternet: 0,
      appreciated: 0,
      recommendation: true
    };
  }

  ngOnInit() {
    this.users = this.usersService.users;
  }

}
